package kawabenchmark;

import static java.util.concurrent.TimeUnit.*;
import java.util.function.Supplier;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import gnu.expr.Language;

public class KawaBenchmark {

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    //@Warmup(iterations = 5, time = 500, timeUnit = MILLISECONDS)
    //@Measurement(iterations = 5, time = 500, timeUnit = MILLISECONDS)
    public void generator1(Blackhole blackhole) throws Exception {
        Language.setCurrentLanguage(Language.getInstance("scheme"));
        var generator1Class = Class.forName("Generator1");
        var generator1 = (Supplier) generator1Class.newInstance();
        Object rez;
        do {
            rez = generator1.get();
            blackhole.consume(rez);
        } while (rez != null);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    //@Warmup(iterations = 5, time = 500, timeUnit = MILLISECONDS)
    //@Measurement(iterations = 5, time = 500, timeUnit = MILLISECONDS)
    public void generator2(Blackhole blackhole) throws Exception {
        Language.setCurrentLanguage(Language.getInstance("scheme"));
        var generator2Class = Class.forName("Generator2");
        var generator2 = (Supplier) generator2Class.newInstance();
        Object rez;
        do {
            rez = generator2.get();
            blackhole.consume(rez);
        } while (rez != null);
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    //@Warmup(iterations = 5, time = 500, timeUnit = MILLISECONDS)
    //@Measurement(iterations = 5, time = 500, timeUnit = MILLISECONDS)
    public void find(Blackhole blackhole) throws Exception {
        Language.setCurrentLanguage(Language.getInstance("scheme"));
        var findClass = Class.forName("Find");
        var find = (Supplier) findClass.newInstance();
        var result = (Boolean) find.get();
        if (!result) {
            throw new RuntimeException();
        }
    }
}
