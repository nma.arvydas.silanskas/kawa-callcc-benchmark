(import (srfi 1))

(define (find/callcc lst pred)
  (call/cc
   (lambda (return)
     (for-each
      (lambda (value)
        (when (pred value)
            (return #t)))
      lst)
     #f)))

(define-simple-class Find (java.util.function.Supplier)
  ((get)
   (find/callcc (iota 10000) (lambda (el) (= el 5000)))))
