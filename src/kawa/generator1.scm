(define (make-generator from)
  (define value from)
  (lambda ()
    (if value
        (let* ((result value)
               (new-value (if (= result 0)
                              #f
                              (- result 1))))
          (set! value new-value)
          result)
        #!null)))

(define-simple-class Generator1 (java.util.function.Supplier)
  (gen init-form: (make-generator 10000))
  ((get)
   (gen)))
